﻿using System;
using System.Collections.Generic;
using System.Configuration.Install;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace fdadb
{
    class Program
    {
        private static string TAG = "FDADB";
        private static string EVENTNAME = "FDADB_20130712";

        private static void logIt(string s)
        {
            Trace.WriteLine($"[{TAG}]: {s}");
        }

        private static int Main(string[] args)
        {
            int result = 0;
            logIt($"{Path.GetFileName(Process.GetCurrentProcess().MainModule.FileName)} start: ++ version: {Process.GetCurrentProcess().MainModule.FileVersionInfo.FileVersion}");
            logIt($"command Line: {Environment.CommandLine}");
            InstallContext installContext = new InstallContext(null, args);
            if (installContext.IsParameterTrue("debug"))
            {
                Console.WriteLine("Wait for debugger, press any key to continue...");
                Console.ReadKey();
            }
            EventWaitHandle eventWaitHandle = null;
            if (installContext.IsParameterTrue("start-server"))
            {
                try
                {
                    eventWaitHandle = EventWaitHandle.OpenExisting(EVENTNAME);
                    logIt("fdadb already running.");
                    eventWaitHandle = null;
                }
                catch (WaitHandleCannotBeOpenedException)
                {
                    eventWaitHandle = new EventWaitHandle(false, EventResetMode.ManualReset, EVENTNAME);
                    Thread thread = new Thread(startServer);
                    thread.IsBackground = true;
                    thread.Start();
                    eventWaitHandle.WaitOne();
                    logIt("fdadb is exiting.");
                }
                catch (Exception)
                {
                    eventWaitHandle = null;
                }
            }
            else if (installContext.IsParameterTrue("kill-server"))
            {
                try
                {
                    eventWaitHandle = EventWaitHandle.OpenExisting(EVENTNAME);
                    eventWaitHandle.Set();
                    eventWaitHandle.Close();
                }
                catch (Exception)
                {
                    eventWaitHandle = null;
                }
            }
            return result;
        }

        private static void startServer(object obj)
        {
            try
            {
                IPEndPoint localEP = new IPEndPoint(IPAddress.Loopback, 5037);
                Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                socket.Bind(localEP);
                socket.Listen(32);
                while (true)
                {
                    Socket handler = socket.Accept();
                    ThreadPool.QueueUserWorkItem(delegate (object o)
                    {
                        Socket socket2 = (Socket)o;
                        try
                        {
                            byte[] array = new byte[1024];
                            int num = handler.Receive(array);
                            string @string = Encoding.ASCII.GetString(array, 0, num);
                            logIt($"[recv]: ({num}) {@string}");
                        }
                        catch (Exception)
                        {
                        }
                        finally
                        {
                            socket2.Shutdown(SocketShutdown.Both);
                            socket2.Close();
                        }
                    }, handler);
                }
            }
            catch (Exception ex)
            {
                logIt(ex.Message);
                logIt(ex.StackTrace);
            }
        }

    }
}
