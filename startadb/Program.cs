﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace startadb
{
    class Program
    {
        static void Main(string[] args)
        {
            System.Configuration.Install.InstallContext cmdLine = new System.Configuration.Install.InstallContext(null, args);
            if (cmdLine.Parameters.ContainsKey("label"))
            {
                try
                {
                    var evt = EventWaitHandle.OpenExisting($"start_adb_label_{cmdLine.Parameters["label"]}");
                    evt.Set();
                }
                catch (Exception e) { Console.WriteLine(e.ToString()); }
            }
            else if (cmdLine.IsParameterTrue("send") && cmdLine.Parameters.ContainsKey("port"))
            {
                int nPort = Convert.ToInt32(cmdLine.Parameters["port"]);
                try
                {
                    TcpClient client = new TcpClient("127.0.0.1", nPort);
                    NetworkStream nwStream = client.GetStream();
                    byte[] bytesToSend = ASCIIEncoding.ASCII.GetBytes("exit");

                    //---send the text---
                    nwStream.Write(bytesToSend, 0, bytesToSend.Length);
                    Thread.Sleep(100);
                    client.Close();
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.ToString());
                }
            }
        }
    }
}
