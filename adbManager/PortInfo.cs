﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Utilities;

namespace adbManager
{
    class PortInfo : IDisposable
    {
        static String sLock = "Lock";
        public int m_nPort;
        public String m_sLoctionPaths;
        //public int m_nPid;
        public int m_nLabel;

        public const int BASE_PORT = 10001;

        private IniFile inifile;
        private String sKey;

        static int nPortRecord = BASE_PORT;

        public Boolean bWaitADB = true;

        private EventWaitHandle exitHolderport = new EventWaitHandle(false, EventResetMode.AutoReset);

        public PortInfo(IniFile ini, String key)
        {
            inifile = ini;
            sKey = key;
        }

        public static bool IsPortBusy(int port)
        {
            bool ret = false;

            IPGlobalProperties ipGP = IPGlobalProperties.GetIPGlobalProperties();
            IPEndPoint[] endpoints = ipGP.GetActiveTcpListeners();

            if (endpoints == null || endpoints.Length == 0)
            {
                goto exit;
            }

            for (int i = 0; i < endpoints.Length; i++)
            {
                if (endpoints[i].Port == port)
                {
                    ret = true;
                    break;
                }
            }

        exit:
            return ret;
        }

        int GetAvailablePort()
        {
            int nPort = nPortRecord;

            IPGlobalProperties ipGP = IPGlobalProperties.GetIPGlobalProperties();
            IPEndPoint[] endpoints = ipGP.GetActiveTcpListeners();

            if (endpoints == null || endpoints.Length == 0)
            {
                return 65536;
            }

            bool bFind = true;
            while(bFind)
            {
                bFind = false;
                lock (sLock)
                {
                    Interlocked.Increment(ref nPortRecord);
                    nPort = nPortRecord;
                }
                for (int i = 0; i < endpoints.Length; i++)
                {
                    if (endpoints[i].Port == nPort)
                    {
                        bFind = true;
                        break;
                    }
                }
            }

            return nPort;
        }

        public static string[] runExe(string exeFilename, string args, out int exitCode, int timeout = 60*1000, string workingDir = "", Dictionary<String, String> sEnvironmentVariable = null)
        {
            List<string> ret = new List<string>();
            exitCode = -1;
            String TAG = "";
            if (sEnvironmentVariable != null && sEnvironmentVariable.Count > 0)
            {
                if (sEnvironmentVariable.ContainsKey("LABELNUMBER"))
                {
                    TAG = sEnvironmentVariable["LABELNUMBER"];
                }
            }
            Program.logIt($"[LABEL_{TAG}]:exe={exeFilename}, args={args}");
            try
            {
                if (System.IO.File.Exists(exeFilename))
                {
                    Program.logIt($"[LABEL_{TAG}]:[runEXE]: {exeFilename} arg={args}");
                    System.Diagnostics.Process p = new System.Diagnostics.Process();
                    p.StartInfo.FileName = exeFilename;
                    p.StartInfo.Arguments = args;
                    p.StartInfo.CreateNoWindow = true;
                    p.StartInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;

                    if (sEnvironmentVariable != null && sEnvironmentVariable.Count > 0)
                    {
                        foreach (var entry in sEnvironmentVariable)
                        {
                            if (p.StartInfo.EnvironmentVariables.ContainsKey(entry.Key as String))
                            {
                                p.StartInfo.EnvironmentVariables[entry.Key as String] = entry.Value as String;
                                Program.logIt($"[LABEL_{TAG}]: replace {entry.Key}=={entry.Value}");
                            }
                            else
                            {
                                p.StartInfo.EnvironmentVariables.Add(entry.Key as String, entry.Value as String);
                                Program.logIt($"[LABEL_{TAG}]: {entry.Key}=={entry.Value}");
                            }
                        }
                    }

                    if (!string.IsNullOrEmpty(workingDir))
                        p.StartInfo.WorkingDirectory = workingDir;
                    p.StartInfo.UseShellExecute = false;
                    p.StartInfo.RedirectStandardOutput = true;
                    p.StartInfo.RedirectStandardError = true;
                    p.OutputDataReceived += (sender, e) =>
                    {
                        if (!string.IsNullOrEmpty(e.Data))
                        {
                            Program.logIt($"[LABEL_{TAG}]: [runEXE]: {e.Data}");
                            ret.Add(e.Data);
                        }
                    };
                    p.ErrorDataReceived += (sender, e) =>
                    {
                        if (!string.IsNullOrEmpty(e.Data))
                        {
                            Program.logIt(string.Format("[runEXE]: {0}", e.Data));
                            ret.Add(e.Data);
                        }
                    };
                    p.Start();
                    p.BeginOutputReadLine();
                    p.BeginErrorReadLine();
                    if (timeout > 0)
                    {
                        p.WaitForExit(timeout);
                        if (!p.HasExited)
                        {
                            p.Kill();
                            exitCode = 1460;
                        }
                        else
                            exitCode = p.ExitCode;

                    }
                    else
                    {
                        exitCode = 0;
                    }
                    Program.logIt($"[LABEL_{TAG}]: [runEXE]: exit code={exitCode}");
                }
                else
                    exitCode = 2;
            }
            catch (Exception e) { Program.logIt($"LABEL_{TAG}:"+e.ToString()); }
            return ret.ToArray();
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void Init(int nLabl, int nPort)
        {
            m_nLabel = nLabl;
            m_nPort = nPort;

            IniFile ini = new IniFile(Path.Combine(Program.getApstHome(), "Calibration.ini"));
            string[] locationss = { "locationpaths", "locationpaths_1.1", "locationpaths_2.0", "locationpaths_3.0" };

            var set = new HashSet<string>(StringComparer.OrdinalIgnoreCase);
            foreach (var sl in locationss)
            {
                String sLP = ini.GetString(sl, Convert.ToString(nLabl), "");
                if (!String.IsNullOrEmpty(sLP))
                {
                    String[] ss = sLP.Split(new char[] { '@' }, StringSplitOptions.RemoveEmptyEntries);
                    if (ss.Length == 2)
                    {
                        //m_sLoctionPaths = String.Format("{0}#USB({1})", ss[1], ss[0]);
                        set.Add(String.Format("{0}#USB({1})", ss[1], ss[0]));
                        Program.logIt(m_sLoctionPaths);
                    }
                    else
                    {
                        Program.logIt("Calibration Data file format error.");
                    }
                }
            }

            if(set.Count == 0)
            { 
                Program.logIt("Location can not find.Calibration Data file format error.");
            }
            else
            {
                m_sLoctionPaths = string.Join(";",set);
            }

            if (!String.IsNullOrEmpty(m_sLoctionPaths))
            {
                inifile.WriteValue("serverport", nLabl.ToString(), nPort);

                var eKeyValue = new Dictionary<String, String>(StringComparer.OrdinalIgnoreCase);
                eKeyValue.Add("LABELNUMBER", Convert.ToString(nLabl));
                eKeyValue.Add("ANDROID_ADB_SERVER_PORT", Convert.ToString(nPort));

                inifile.WriteValue("label_" + nLabl.ToString(), "ANDROID_ADB_SERVER_PORT", nPort);
                inifile.WriteValue("label_" + nLabl.ToString(), "LABELNUMBER", nLabl);

                eKeyValue.Add("LABELPATH", m_sLoctionPaths);
                inifile.WriteValue("label_" + nLabl.ToString(), "LABELPATH", m_sLoctionPaths);
                StartWork(eKeyValue);
            }
        }
        public void ExitWait(int port)
        {
            if (!bWaitADB) return;
            //int exitcode;
            //runExe(Environment.ExpandEnvironmentVariables("%APSTHOME%startadb.exe"), $"-send -port={port}", out exitcode);
            try
            {
                TcpClient client = new TcpClient("127.0.0.1", port);
                NetworkStream nwStream = client.GetStream();
                byte[] bytesToSend = ASCIIEncoding.ASCII.GetBytes("exit");

                //---send the text---
                nwStream.Write(bytesToSend, 0, bytesToSend.Length);
                Thread.Sleep(100);
                client.Close();
                bWaitADB = true;
            }
            catch (Exception e)
            {
                Program.logIt(e.ToString());
            }

        }
        private static void startServer(int port)
        {
            TcpListener server = null;
            try
            {
                Boolean bExit = false;
                IPAddress localAddr = IPAddress.Parse("127.0.0.1");
                server = new TcpListener(localAddr, port);
                // Start listening for client requests.
                server.Start();
                byte[] array = new byte[1024];
                while (!bExit)
                {
                    using (TcpClient client = server.AcceptTcpClient())
                    {
                        int i = 0;
                        NetworkStream stream = client.GetStream();
                        if ((i = stream.Read(array, 0, array.Length)) != 0)
                        {
                            string sret = Encoding.ASCII.GetString(array, 0, i);
                            Program.logIt($"[recv]: (len={i}) {sret}");
                            if (sret.StartsWith("exit", StringComparison.CurrentCultureIgnoreCase))
                            {
                                Program.logIt("exiting...");
                                bExit = true;
                            }
                        }
                    }
                }
                /*
                //IPEndPoint localEP = new IPEndPoint(IPAddress.Loopback, port);
                //using (Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp))
                //{
                //    socket.Bind(localEP);
                //    socket.Listen(1);
                //    while (!bExit)
                //    {
                //        Socket handler = socket.Accept();
                //        try
                //        {//only recv one package data
                //            byte[] array = new byte[1024];
                //            int num = handler.Receive(array);
                //            string sret = Encoding.ASCII.GetString(array, 0, num);
                //            Program.logIt($"[recv]: ({num}) {sret}");
                //            if (sret.StartsWith("exit", StringComparison.CurrentCultureIgnoreCase))
                //            {
                //                Program.logIt("exiting...");
                //                bExit = true;
                //            }
                //        }
                //        catch (Exception)
                //        {
                //        }
                //        finally
                //        {
                //            handler.Shutdown(SocketShutdown.Both);
                //            handler.Close();
                //            Program.logIt("exiting thread");
                //            socket.Close();
                //        }
                //    }
                //    Program.logIt($"server thread exit {port}");
                //    socket.Close();
                //}
                */
            }
            catch (Exception ex)
            {
                Program.logIt(ex.Message);
                Program.logIt(ex.StackTrace);
            }
            finally
            {
                server.Server.Close();
                server.Stop();
                server.Server.Close();
                server = null;
                GC.Collect();
            }
            
        }
        public void StartWork(Dictionary<String, String> eKeyValue)
        {
            //read calibration.
            if (!Program.ADBStart)
            {
                EventWaitHandle eventWaitHandle = new EventWaitHandle(false, EventResetMode.ManualReset, $"start_adb_label_{m_nLabel}");
                //Thread thread = new Thread(() => { startServer(m_nPort); });
                //thread.IsBackground = true;
                //thread.Name = $"start_adb_label_{m_nLabel}";
                //thread.Start();
                eventWaitHandle.WaitOne();
                ExitWait(m_nPort);
                //thread.Abort();
                eventWaitHandle.Close();
            }
            bWaitADB = false;
           if (!Program.ExitSystem)
            {//write port

                int exitCode;
                String[] ret = runExe(Path.Combine(Program.getApstHome(), "adb.exe"), "start-server", out exitCode, 10 * 1000, "", eKeyValue);
                foreach (String s in ret)
                {
                    //* daemon not running. starting it now on port 5037 *
                    //* daemon started successfully *
                    Program.logIt(s);
                }
            }

        }

        public void UnInit(int nLabl, int nPort)
        {
            int exitCode;
            var eKeyValue = new Dictionary<String, String>(StringComparer.OrdinalIgnoreCase);
            eKeyValue.Add("LABELNUMBER", Convert.ToString(nLabl));
            eKeyValue.Add("ANDROID_ADB_SERVER_PORT", Convert.ToString(nPort));

            runExe(Path.Combine(Program.getApstHome(), "adb.exe"), "kill-server", out exitCode, 2 * 1000, "", eKeyValue);
        }


        public void ThreadRunExe()
        {
            int nPort = GetAvailablePort();
            if (nPort > 65535)
            {
                Program.logIt("Can not get Available port " + nPort);
                return;
            }
            m_nPort = nPort;
            Init(Convert.ToInt32(sKey), nPort);  
        }

        public void Dispose()
        {
           //adb kill-server
            m_sLoctionPaths = "";
        }
    }
}
