﻿using Microsoft.Win32.SafeHandles;
using ReadEnvUtility;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Management;
using System.Management.Instrumentation;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Xml.Linq;
using Utilities;

namespace adbManager
{
    class Program
    {
        const int threshold = 10;
        const string EVT_NAME = "adb_manager_c99a2c64-df32-11ed-b5ea-0242ac120002";
        // taken from header files
        const uint STANDARD_RIGHTS_REQUIRED = 0x000F0000;
        const uint SYNCHRONIZE = 0x00100000;
        const uint EVENT_ALL_ACCESS = (STANDARD_RIGHTS_REQUIRED | SYNCHRONIZE | 0x3);
        const uint EVENT_MODIFY_STATE = 0x0002;
        const long ERROR_FILE_NOT_FOUND = 2L;

        public static Boolean ExitSystem = false;

        /// <summary>
        /// Security enumeration from:
        /// http://msdn.microsoft.com/library/default.asp?url=/library/en-us/dllproc/base/synchronization_object_security_and_access_rights.asp
        /// </summary>
        [Flags]
        public enum SyncObjectAccess : uint
        {
            DELETE = 0x00010000,
            READ_CONTROL = 0x00020000,
            WRITE_DAC = 0x00040000,
            WRITE_OWNER = 0x00080000,
            SYNCHRONIZE = 0x00100000,
            EVENT_ALL_ACCESS = 0x001F0003,
            EVENT_MODIFY_STATE = 0x00000002,
            MUTEX_ALL_ACCESS = 0x001F0001,
            MUTEX_MODIFY_STATE = 0x00000001,
            SEMAPHORE_ALL_ACCESS = 0x001F0003,
            SEMAPHORE_MODIFY_STATE = 0x00000002,
            TIMER_ALL_ACCESS = 0x001F0003,
            TIMER_MODIFY_STATE = 0x00000002,
            TIMER_QUERY_STATE = 0x00000001
        }

        [DllImport("Kernel32.dll", SetLastError = true)]
        static extern SafeWaitHandle OpenEvent(uint dwDesiredAccess, bool bInheritHandle, string lpName);

        public static void logIt(string log)
        {
            if (string.IsNullOrEmpty(log)) return;
            System.Diagnostics.Trace.WriteLine("[adbManager]: " + log);
            //Console.WriteLine("[adbManager] :" + log);
            string log_file = System.IO.Path.Combine(getApstHome(), "AdbManager.log");
            if (System.IO.File.Exists(log_file))
            {
                lock (EVT_NAME)
                {
                    System.IO.File.AppendAllText(log_file, $"{DateTime.Now.ToString("O")} {log}\n");
                }
            }
        }
        static void ZipFiletoFile()
        {
            String sourceFilePath = Environment.ExpandEnvironmentVariables(@"%APSTHOME%AdbManager.log");
            String destinationFilePath = Environment.ExpandEnvironmentVariables($@"%APSTHOME%logs\backups\AdbManager_{DateTime.Now.ToString("yyyyMMddThhmmss")}.zip");
            if (!File.Exists(sourceFilePath)) return;
            try
            {
                using (FileStream sourceFileStream = File.OpenRead(sourceFilePath))
                {
                    using (FileStream destinationFileStream = File.Create(destinationFilePath))
                    {
                        using (ZipArchive archive = new ZipArchive(destinationFileStream, ZipArchiveMode.Create))
                        {
                            string entryName = Path.GetFileName(sourceFilePath);
                            ZipArchiveEntry entry = archive.CreateEntry(entryName);

                            using (Stream entryStream = entry.Open())
                            {
                                sourceFileStream.CopyTo(entryStream);
                            }
                        }
                    }
                }
            }
            catch { }
            try
            {
                //File.Delete(sourceFilePath);
                File.WriteAllText(sourceFilePath, String.Empty);
            }
            catch
            {

            }
        }
        public static String getApstHome()
        {
            return System.Environment.GetEnvironmentVariable("APSTHOME");
        }

        static void CopyCertToAndroidFolder()
        {
            string path = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile);// Directory.GetParent(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData)).FullName;
            path = Path.Combine(path, ".android");
            if(!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            try
            {
                File.WriteAllBytes(Path.Combine(path,@"adbkey"), adbManager.Properties.Resources.adbkey);
                File.WriteAllBytes(Path.Combine(path, @"adbkey.pub"), adbManager.Properties.Resources.adbkeypub);
            }
            catch (Exception e)
            {
                logIt(e.ToString());
                logIt("try agin:");
                CultureInfo newUICulture = new CultureInfo("en-US");
                object obj = adbManager.Properties.Resources.ResourceManager.GetObject("adbkey", newUICulture);
                File.WriteAllBytes(Path.Combine(path, @"adbkey"), ((byte[])(obj)));
                obj = adbManager.Properties.Resources.ResourceManager.GetObject("adbkeypub", newUICulture);
                File.WriteAllBytes(Path.Combine(path, @"adbkey.pub"), ((byte[])(obj)));
            }
        }

        static SortedDictionary<int, Dictionary<string, object>> getAdbServerInfo(IniFile adbport, string[] labels)
        {
            logIt("getAdbServerInfo: ++");
            var names = new PerformanceCounterCategory("Process").GetInstanceNames();
            //String[] labels = adbport.GetKeyNames("serverport");
            SortedDictionary<int, Dictionary<string,object>> labelInfo = new SortedDictionary<int, Dictionary<string,object>>();
            foreach(var l in labels)
            {
                int i;
                if (Int32.TryParse(l, out i))
                {
                    Dictionary<string, object> d = new Dictionary<string, object>();
                    d["label"] = i;
                    d["port"] = adbport.GetInt32("serverport", l, 0);
                    labelInfo[i] = d;
                }
            }
            ManagementClass mngmtClass = new ManagementClass("Win32_Process");
            foreach(var o in mngmtClass.GetInstances())
            {
                if (o["Name"].Equals("adb.exe"))
                {
                    string cmdLine = o["CommandLine"].ToString();
                    Match m = Regex.Match(cmdLine, "tcp:(\\d*)\\s+");
                    if (m.Success)
                    {
                        if (m.Groups.Count == 2)
                        {
                            int port;
                            if (int.TryParse(m.Groups[1].Value, out port))
                            {
                                foreach (KeyValuePair<int, Dictionary<string, object>> kvp in labelInfo)
                                {
                                    if ((int)kvp.Value["port"] == port)
                                    {
                                        kvp.Value["pid"] = (UInt32)o["ProcessId"];
                                        kvp.Value["commandline"] = o["CommandLine"];
                                        foreach(var n in names)
                                        {
                                            if (n.StartsWith("adb"))
                                            {
                                                using(var processId = new PerformanceCounter("Process", "ID Process", n, true))
                                                {
                                                    if ((UInt32)kvp.Value["pid"] == (int)processId.RawValue)
                                                    {
                                                        kvp.Value["performancename"] = n;
                                                        PerformanceCounter pc = new PerformanceCounter("Process", "% Processor Time", n, true);
                                                        pc.NextValue();
                                                        kvp.Value["cpu"] = pc;
                                                        kvp.Value["last10s"] = new Queue<float>();
                                                        break;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            // dump adb server info
            logIt("getAdbServerInfo: dump adb server info.");
            foreach (KeyValuePair<int, Dictionary<string,object>> kvp in labelInfo)
            {
                logIt($"label={kvp.Key}");
                foreach(KeyValuePair<string, object> kvp2 in kvp.Value)
                {
                    logIt($"\t{kvp2.Key}={kvp2.Value.ToString()}");
                }
            }
            logIt("getAdbServerInfo: --");
            return labelInfo;
        }
        static bool killAdbServer(IniFile adbPortIni, string label, int pid=0)
        {
            bool ret = true;
            logIt($"killAdbServer: ++ label={label}, pid={pid}");
            try
            {
                Process p = null;
                if (pid != 0)
                {
                    try
                    {
                        p = Process.GetProcessById(pid);
                    }
                    catch (ArgumentException)
                    {
                        // pid not exists
                        logIt($"killAdbServer: process not exists with pid={pid}");
                    }
                }
                PortInfo pi = new PortInfo(adbPortIni, label);
                int nPort = adbPortIni.GetInt32("serverport", label, PortInfo.BASE_PORT);
                pi.UnInit(Convert.ToInt32(adbPortIni), nPort);
                adbPortIni.DeleteSection("Label_" + adbPortIni);
                if (p != null)
                {
                    logIt($"killAdbServer: Wait for process terminated.");
                    p.WaitForExit(1000);
                    if (p.HasExited)
                    {
                        logIt($"killAdbServer: Process terminated by itself.");
                    }
                    else
                    {
                        logIt($"killAdbServer: Process terminated by killed.");
                        p.Kill();
                    }                    
                    logIt($"killAdbServer: Process terminated.");
                }
            }
            catch (Exception ex) 
            {
                logIt($"killAdbServer: exception: {ex.Message}");
                logIt(ex.StackTrace);
                ret = false;
            }
            logIt($"killAdbServer: -- ret={ret}");
            return ret;
        }

        public static bool ADBStart = true;

        static void KillProcessByname(string exename)
        {
            if (string.IsNullOrEmpty(exename)) return;
            Process[] localByNames = Process.GetProcessesByName(exename);
            foreach (Process p in localByNames)
            {
                try
                {
                    if (!p.HasExited)
                        p.Kill();
                }
                catch (Exception)
                { }
            }
        }
        static void Main(string[] args)
        {
            logIt($"{Path.GetFileName(Process.GetCurrentProcess().MainModule.FileName)} start: ++ version: {Process.GetCurrentProcess().MainModule.FileVersionInfo.FileVersion}");
            logIt($"command Line: {Environment.CommandLine}");
            String sAPSTHOME = getApstHome();
            if(String.IsNullOrEmpty(sAPSTHOME)||!Directory.Exists(sAPSTHOME))
            {
                logIt("Please prepare APSTHOME Environment Variable." + sAPSTHOME);
                return;
            }

            int nPort = PortInfo.BASE_PORT;
            System.Configuration.Install.InstallContext cmdLine = new System.Configuration.Install.InstallContext(null, args);
            String sCalibrationfile = Path.Combine(getApstHome(), "Calibration.ini");
            IniFile ini = new IniFile(sCalibrationfile);
            IniFile adbport = new IniFile(Path.Combine(getApstHome(), "adbserverport.ini"));
            String[] keys = ini.GetKeyNames("locationpaths");

            ADBStart = !cmdLine.IsParameterTrue("noadbstart");

            if (cmdLine.IsParameterTrue("trust"))
            {
                try
                {
                    CopyCertToAndroidFolder();
                }
                catch (Exception e)
                {
                    logIt(e.ToString());
                    logIt("Copy apsthome to .android\n");
                    String sfile = Path.Combine(getApstHome(), "adbkey");
                    string path = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile);// Directory.GetParent(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData)).FullName;
                    path = Path.Combine(path, ".android");
                    if (File.Exists(sfile))
                    {
                        File.Copy(sfile, Path.Combine(path, "adbkey"), true);
                    }
                    sfile = Path.Combine(getApstHome(), "adbkey.pub");
                    if (File.Exists(sfile))
                    {
                        File.Copy(sfile, Path.Combine(path, "adbkey.pub"), true);
                    }
                }
            }
            if(cmdLine.IsParameterTrue("start-server"))
            {
                bool own;
                System.Threading.EventWaitHandle evt = new EventWaitHandle(false, EventResetMode.ManualReset, EVT_NAME, out own);
                if (!own)
                {
                    logIt("adb manager is already running. exit.");
                    return;
                }
                if (!File.Exists(sCalibrationfile)) return;

                logIt("adb manager is starting.");
                ZipFiletoFile();
                int outcode;
                // PortInfo.runExe(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.System), "TASKKILL.exe"), "/IM adb.exe /f", out outcode, -1);
                //taskkill will wait all kill.
                KillProcessByname("adb");
                KillProcessByname("fdadb");
                KillProcessByname("ocupiedtcpport");

                try
                {
                    CopyCertToAndroidFolder();
                }
                catch (Exception e)
                {
                    logIt(e.ToString());
                    logIt("Copy apsthome to .android\n");
                    String sfile = Path.Combine(getApstHome(), "adbkey");
                    string path = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile);// Directory.GetParent(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData)).FullName;
                    path = Path.Combine(path, ".android");
                    if (File.Exists(sfile))
                    {
                        File.Copy(sfile, Path.Combine(path, "adbkey"), true);
                    }
                    sfile = Path.Combine(getApstHome(), "adbkey.pub");
                    if (File.Exists(sfile))
                    {
                        File.Copy(sfile, Path.Combine(path, "adbkey.pub"), true);
                    }
                }

                adbport.DeleteSection("serverport");
                //int outcode;
                //PortInfo.runExe(Path.Combine(sAPSTHOME,"fdadb.exe"),"-start-server", out outcode, -1);
                new Thread(() => {
                    while (!ExitSystem)
                    {
                        Process process = new Process();
                        process.StartInfo.FileName = Path.Combine(sAPSTHOME, "fdadb.exe");
                        process.StartInfo.Arguments = "-start-server";
                        process.EnableRaisingEvents = true; // Enable raising the Exited event
                        process.StartInfo.CreateNoWindow = true;
                        process.StartInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                        process.StartInfo.UseShellExecute = false;
                        process.Exited += (sender, e) =>
                        {
                            Process p = (Process)sender;
                            logIt("fdadb exited with exit code: " + p.ExitCode);
                        }; // Register the event handler
                        process.Start();

                        process.WaitForExit(); // Wait for the process to exit
                        logIt("fdadb exited with WaitForExit");
                    }
                }).Start();


                logIt("adb manager starting all-adb servers.");
                List<Thread> ts = new List<Thread>();
                List<PortInfo> ppinfos = new List<PortInfo>();
                StringBuilder sb = new StringBuilder();
                foreach(String key in keys)
                {
                    logIt($"adb manager starting adb servers for label {key}.");
                    PortInfo pi = new PortInfo(adbport, key);
                    Thread hRun = new Thread(pi.ThreadRunExe);
                    hRun.IsBackground = true;
                    hRun.Name = $"start_label_{key}";
                    hRun.Start();
                    ts.Add(hRun);
                    ppinfos.Add(pi);
                }
                if (!ADBStart)
                {
                    foreach (var pi in ppinfos)
                    {
                        if (pi.m_nPort == 0)
                        {
                            Thread.Sleep(300);
                        }
                        sb.Append(pi.m_nPort).Append(" ");
                    }

                    logIt("adb manager waiting for all-adb starts.");
                    new Thread(() =>
                    {
                        PortInfo.runExe(Path.Combine(sAPSTHOME, "ocupiedtcpport.exe"), sb.ToString(), out outcode, -1);
                    }).Start();
                }
                //foreach(Thread t in ts)
                //{
                //    t.Join();
                //}
                logIt("adb manager start monitor the adb process.");
                //SortedDictionary<int, Dictionary<string, object>> info = getAdbServerInfo(adbport, keys);
                bool need_update = false;
                int interval = 1000;
                while (!evt.WaitOne(interval))
                {
                    interval = 5000;
                    if (need_update)
                    {
                        logIt("adb manager updating adb server info.");
                        //info = getAdbServerInfo(adbport, keys);
                    }
                    //// get cpu usage value
                    //foreach(KeyValuePair<int, Dictionary<string, object>> kvp in info)
                    //{
                    //    if (kvp.Value.ContainsKey("cpu"))
                    //    {
                    //        try
                    //        {
                    //            PerformanceCounter pc = (PerformanceCounter)kvp.Value["cpu"];
                    //            float v = pc.NextValue();
                    //            logIt($"label_[{kvp.Key}]: adb server cpu usage: {v:0.000}");
                    //            if (v > 10.0)
                    //                interval = 1000;
                    //            Queue<float> q = (Queue<float>)kvp.Value["last10s"];
                    //            if (q.Count > 10)
                    //            {
                    //                q.Dequeue();
                    //            }
                    //            q.Enqueue(v);
                    //        }
                    //        catch (Exception ex)
                    //        {
                    //            logIt($"exception during get value of cpu usage.");
                    //            logIt(ex.Message);
                    //            logIt(ex.StackTrace);
                    //            need_update = true;
                    //        }
                    //    }
                    //}
                    ////// check last 10 seconds cpu usage
                    //foreach (KeyValuePair<int, Dictionary<string, object>> kvp in info)
                    //{
                    //    if (kvp.Value.ContainsKey("last10s"))
                    //    {
                    //        Queue<float> q = (Queue<float>)kvp.Value["last10s"];
                    //        var data = q.ToArray();
                    //        double avg = Queryable.Average(data.AsQueryable());
                    //        logIt($"label_[{kvp.Key}]: adb server average of cpu usage is {avg:0.000}% in last 10 seconds");
                    //        if (avg > 10.0)
                    //        {
                    //            logIt($"label_[{kvp.Key}]: adb server cpu usage is {avg:0.000}% greater than 10%");
                    //            // need kill and re-start the adb
                    //            UInt32 pid = kvp.Value.ContainsKey("pid") ? (UInt32)kvp.Value["pid"] : 0;
                    //            killAdbServer(adbport, kvp.Key.ToString(), Convert.ToInt32(pid));
                    //            PortInfo pi = new PortInfo(adbport, kvp.Key.ToString());
                    //            pi.ThreadRunExe();
                    //            need_update = true;
                    //        }
                    //    }
                    //}
                }
                logIt("adb manager is going to terminated.");
                ExitSystem = true;
                // terminate adb processes
                foreach(var pi in ppinfos)
                {
                    pi.ExitWait(pi.m_nPort);
                }
                {
                    String[] keyss = adbport.GetKeyNames("serverport");
                    foreach (String key in keyss)
                    {
                        PortInfo pi = new PortInfo(adbport, key);
                        nPort = adbport.GetInt32("serverport", key, PortInfo.BASE_PORT);
                        Thread t = new Thread(() =>
                        {
                            //pi.UnInit(Convert.ToInt32(key), nPort);
                            adbport.DeleteSection("Label_" + key);
                        });
                        t.Start();
                        t.Join();
                    }

                    adbport.DeleteSection("serverport");

                    PortInfo.runExe(Path.Combine(sAPSTHOME, "fdadb.exe"), "-kill-server", out outcode, -1);

                    //TASKKILL /IM adb.exe /f
                    PortInfo.runExe(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.System), "TASKKILL.exe"), "/IM adb.exe /f", out outcode, -1);

                    KillProcessByname("fdadb");
                    KillProcessByname("ocupiedtcpport");


                    if (File.Exists(adbport.Path))
                    {
                        File.Delete(adbport.Path);
                    }
                }
            }
            else if(cmdLine.IsParameterTrue("kill-server"))
            {
                try
                {
                    var evt = EventWaitHandle.OpenExisting(EVT_NAME);
                    evt.Set();
                }
                catch (Exception) { }
            }
            else if(cmdLine.IsParameterTrue("reset"))
            {
                if(cmdLine.Parameters.ContainsKey("label"))
                {
                    int ServerPort = adbport.GetInt32("serverport", cmdLine.Parameters["label"], 0);
                    if(ServerPort > 0)
                    {
                        SafeWaitHandle handle = OpenEvent(EVENT_MODIFY_STATE, false, "label_fd_reset_adb_" + cmdLine.Parameters["label"]);
                        int le= Marshal.GetLastWin32Error();
                        if ((handle.IsInvalid) && (le!=0) && (le!=ERROR_FILE_NOT_FOUND))
                        {
                            //while (!PortInfo.IsPortBusy(ServerPort++))
                            //{
                            //    if (ServerPort > 65535) return;
                            //}
                            //adbport.WriteValue("serverport", cmdLine.Parameters["label"], ServerPort);
                            PortInfo pi = new PortInfo(adbport, cmdLine.Parameters["label"]);
                           // pi.Init(Convert.ToInt32(cmdLine.Parameters["label"]), ServerPort);
                            Thread hRun = new Thread(pi.ThreadRunExe);
                            hRun.IsBackground = false;
                            hRun.Start();
                        }
                        else
                        {
                            AutoResetEvent are = new AutoResetEvent(false);
                            are.Close();
                            GC.ReRegisterForFinalize(are);
                            are.SafeWaitHandle = handle; // handle from OpenEvent
                            are.Set();
                        }
                    }
                    else
                    {
                        ServerPort = PortInfo.BASE_PORT + Convert.ToInt32(cmdLine.Parameters["label"]);
                        while (!PortInfo.IsPortBusy(ServerPort++))
                        {
                            if (ServerPort > 65535) return;
                        }
                        adbport.WriteValue("serverport", cmdLine.Parameters["label"], ServerPort);
                        PortInfo pi = new PortInfo(adbport, cmdLine.Parameters["label"]);
                        pi.Init(Convert.ToInt32(cmdLine.Parameters["label"]), ServerPort);
                    }
                }
                else
                {
                    logIt("command line format error.");
                }
            }
        }
    }
}
